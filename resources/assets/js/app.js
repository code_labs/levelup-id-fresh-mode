import {
  TweenMax
} from 'gsap';
import curDot from '../js/components/cursor';

var WOW = require('wowjs').WOW;
var jQuery = require('jquery');
window.jQuery = jQuery;
window.$ = jQuery;
require("./bootstrap");
require("intersection-observer");
var LazyLoad = require("vanilla-lazyload");
require("slick-carousel");
require("../js/components/swiper");


var wow = new WOW({
  boxClass: 'wow', // animated element css class (default is wow)
  animateClass: 'animated', // animation css class (default is animated)
  offset: 0, // distance to the element when triggering the animation (default is 0)
  mobile: true, // trigger animations on mobile devices (default is true)
  live: false, // act on asynchronously loaded content (default is true)
  callback: function (box) {
    // the callback is fired every time an animation is started
    // the argument that is passed in is the DOM node being animated
  },
  scrollContainer: null // optional scroll container selector, otherwise use window
});
wow.init();

$(document).ready(function () {

  // LazyLoad
  var lazyLoadInstance = new LazyLoad({
    elements_selector: "[data-src]"
    // ... more custom settings?
  });

  var lazyLoadInstancebg = new LazyLoad({
    elements_selector: "[data-bg]"
    // ... more custom settings?// Assign the callbacks defined above
  });

  if (lazyLoadInstance && lazyLoadInstancebg) {
    lazyLoadInstance.update();
    lazyLoadInstancebg.update();
  };
  // End Lazy Load

  // Slick with lazyload
  // $(".hero").slick({
  //   dots: true,
  //   infinite: true,
  //   speed: 600,
  //   fade: !0,
  //   cssEase: "linear",
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   // autoplay: true,
  //   autoplaySpeed: 8000,
  //   draggable: false,
  //   arrows: true,
  //   nextArrow: '<div class="carousel-arrow right"><div class="arrow next"><img src="img/arrow.svg" alt=""></div></div>',
  //   prevArrow: '<div class="carousel-arrow left"><div class="arrow prev"><img src="img/arrow.svg" alt=""></div></div>',
  //   responsive: [{
  //       breakpoint: 1024,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1,
  //         infinite: true,
  //       }
  //     },
  //     {
  //       breakpoint: 768,
  //       settings: {
  //         draggable: true,
  //       }
  //     },
  //     {
  //       breakpoint: 600,
  //       settings: {
  //         slidesToShow: 1,
  //         draggable: true,
  //         slidesToScroll: 1,
  //       }
  //     },
  //     {
  //       breakpoint: 480,
  //       settings: {
  //         slidesToShow: 1,
  //         draggable: true,
  //         slidesToScroll: 1
  //       }
  //     }
  //   ]
  // });

  // centermodevar $st = $('.pagination');
  $('.work-carousel')
    .on('afterChange init', function (event, slick, direction) {
      // console.log('afterChange/init', event, slick, slick.$slides);
      // remove all prev/next
      slick.$slides.removeClass('prevSlide').removeClass('nextSlide');

      // find current slide
      for (var i = 0; i < slick.$slides.length; i++) {
        var $slide = $(slick.$slides[i]);
        if ($slide.hasClass('slick-current')) {
          // update DOM siblings
          $slide.prev().addClass('prevSlide');
          $slide.next().addClass('nextSlide');
          break;
        }
      }
    })
    .on('beforeChange', function (event, slick) {
      // optional, but cleaner maybe
      // remove all prev/next
      slick.$slides.removeClass('prevSlide').removeClass('nextSlide');
    })
    .slick({
      speed: 900,
      cssEase: "",
      centerMode: !0,
      centerPadding: "4%",
      lazyLoad: "progressive",
      slidesToShow: 1,
      infinite: false,
      nextArrow: '<div class="carousel-arrow right"><div class="arrow next"><img src="img/arrow.svg" alt=""></div></div>',
      prevArrow: '<div class="carousel-arrow left"><div class="arrow prev"><img src="img/arrow.svg" alt=""></div></div>',
    });


  // 

  $(".work-carousel-mobile").slick({
    infinite: false,
    slidesToShow: 1,
    draggable: true,
    arrows: false,
  });

  $(".mobile-carousel-business").slick({
    infinite: false,
    slidesToShow: 1,
    draggable: true,
    arrows: false,
  });

  $(".mobile-carousel-market").slick({
    infinite: false,
    slidesToShow: 1,
    draggable: true,
    arrows: false,
  });


  //   End Slick
});


$(document).ready(function () {
  // swiper
  var swiper = new Swiper('.swiper-container', {
    pagination: '.swiper-pagination',
    slidesPerView: 2,
    centeredSlides: true,
    paginationClickable: true,
    spaceBetween: 50,
    loop: false,
    slideToClickedSlide: true,
    observer: true,
    observeParents: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });

  swiper.slideTo(1, false, false);

  // paroller
  // initialize paroller.js and set attributes for selected elements
  $(".paroller, [data-paroller-factor]").paroller({
    factor: 0.1, // multiplier for scrolling speed and offset
    factorXs: 0.1, // multiplier for scrolling speed and offset
    type: 'foreground', // background, foreground
    direction: 'horizontal', // vertical, horizontal
    transition: 'transform 400ms cubic-bezier(0.235, 0.615, 0.185, 0.995)' // CSS transition
  });

})

// curosr
// cursor
const cursor = curDot({});

cursor.over(".title", {
  background: "yellow",
  borderColor: 'transparent',
  scale: 1.2
});

// cursor.over(".splash", {
//   background: "yellow",
//   borderColor: 'orange',
//   scale: .6
// });

cursor.over("a", {
  background: "yellow",
  borderColor: 'transparent',
  scale: .8
});

cursor.over(".offer-desc", {
  background: "yellow",
  borderColor: 'transparent',
  scale: .8
});

cursor.over(".title-white", {
  background: "green",
  borderColor: 'transparent',
  scale: .8
});


// modal
$(document).ready(function () {

  // Gets the video src from the data-src on each button

  var $videoSrc;
  var $slideSrc
  $('.video-btn').click(function () {
    $videoSrc = $(this).data("src");
  });
  $('.slide-btn').click(function () {
    $slideSrc = $(this).data("src");
  });
  console.log($videoSrc);
  console.log($slideSrc);



  // when the modal is opened autoplay it  
  $('#myModal').on('shown.bs.modal', function (e) {

    function getId(url) {
      const regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|&v=)([^#&?]*).*/;
      const match = url.match(regExp);

      return (match && match[2].length === 11) ?
        match[2] :
        null;
    }

    const videoId = getId($videoSrc);

    console.log('Video ID:', videoId)

    // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
    $("#video").attr('src', "http://www.youtube.com/embed/" + videoId);
  })

  // when the modal is opened autoplay it  
  $('#myModal2').on('shown.bs.modal', function (e) {

    // set the video src to autoplay and not to show related video. Youtube related video is like a box of chocolates... you never know what you're gonna get
    $("#gslide").attr('src', $slideSrc);
  })



  // stop playing the youtube video when I close the modal
  $('#myModal').on('hide.bs.modal', function (e) {
    // a poor man's stop video
    $("#video").attr('src', $videoSrc);
  })





  // document ready  
});




// mouse
$('html').mousemove(function (e) {

  var wx = $(window).width();
  var wy = $(window).height();

  var x = e.pageX - this.offsetLeft;
  var y = e.pageY - this.offsetTop;

  var newx = x - wx / 2;
  var newy = y - wy / 2;

  $('#e-wrapper div').each(function () {
    var speed = $(this).attr('data-speed');
    if ($(this).attr('data-revert')) speed *= -1;
    TweenMax.to($(this), 1, {
      x: (1 - newx * speed),
      y: (1 - newy * speed)
    });

  });

});




// navbar on scroll

$(function () {
  $(document).scroll(function () {
    var $nav = $(".navbar");
    $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
  });
});



// refresh button and nav
var lastScrollTop1 = 0;
$(window).scroll(function () {
  var st = $(this).scrollTop();
  var refresh = $('.refresh-btn');
  var banner = $('.navbar');
  setTimeout(function () {
    if (st > lastScrollTop1) {
      refresh.removeClass('hide');
      banner.addClass('hide');
    } else {
      refresh.addClass('hide');
      banner.removeClass('hide');
    }
    lastScrollTop1 = st;
  }, 100);
});



// navbar active

$('#topheader .navbar-nav a').on('click', function () {
  $('#topheader .navbar-nav').find('li.active').removeClass('active');
  $(this).parent('li').addClass('active');
});


// chechbox nav
const checkbox = document.getElementById('open');
const link1 = document.getElementById('link1');
const link2 = document.getElementById('link2');
const link3 = document.getElementById('link3');
const link4 = document.getElementById('link4');

function toggleNavbarMobile() {
  // alert('you clicked the secondary button');
  checkbox.click();
}

link1.addEventListener("click", toggleNavbarMobile, false);
link2.addEventListener("click", toggleNavbarMobile, false);
link3.addEventListener("click", toggleNavbarMobile, false);
link4.addEventListener("click", toggleNavbarMobile, false);


// rotate in scroll 
// Source credit: http://thenewcode.com/279/Rotate-Elements-on-Scroll-with-JavaScript
$(window).scroll(function () {
  var theta = $(window).scrollTop() / 10 % Math.PI;
  $('#reload').css({
    transform: 'rotate(-' + theta + 'rad)'
  });
});


// tab
const primary = document.getElementById("deleteme");
const secondary = document.getElementById("clicktodelete");
const third = document.getElementById("clicktodelete2");

function primaryHidden() {
  if (primary.classList.contains("onclick-d-none")) {
    primary.classList.remove("onclick-d-none");
    primary.classList.add("d-none");
  };
};

secondary.addEventListener("click", primaryHidden, false);
third.addEventListener("click", primaryHidden, false);



// // stars
// "use strict";

// var canvas = document.getElementById('canvas'),
//   ctx = canvas.getContext('2d'),
//   w = canvas.width = window.innerWidth,
//   h = canvas.height = window.innerHeight,

//   hue = 86,
//   stars = [],
//   count = 0,
//   maxStars = 1300;

// // Thanks @jackrugile for the performance tip! https://codepen.io/jackrugile/pen/BjBGoM
// // Cache gradient
// var canvas2 = document.createElement('canvas'),
//   ctx2 = canvas2.getContext('2d');
// canvas2.width = 100;
// canvas2.height = 100;
// var half = canvas2.width / 4,
//   gradient2 = ctx2.createRadialGradient(half, half, 0, half, half, half);
// gradient2.addColorStop(0.025, '#fff');
// gradient2.addColorStop(0.1, 'hsl(' + hue + ', 1%, 13%)');
// gradient2.addColorStop(0.25, 'hsl(' + hue + ', 4%, 6%)');
// gradient2.addColorStop(1, 'transparent');

// ctx2.fillStyle = gradient2;
// ctx2.beginPath();
// ctx2.arc(half, half, half, 0, Math.PI * 2);
// ctx2.fill();

// // End cache